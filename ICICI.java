public class ICICI extends SavingsAccount {

    public ICICI(String name, String email, double initialAccountBalance){
        super(name, email, initialAccountBalance);
    }

    public ICICI(String name, String email){
        super(name, email);
    }

    public String userDetails(){
        return "ICICI user details : "+super.getUser() + " Email: "+ super.getEmail();
    }
    
}
