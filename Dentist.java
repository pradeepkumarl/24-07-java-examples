public class Dentist  extends Doctor {

    public Dentist(String name, int age){
        super(name, age);
    }

    @Override
    public void treatPatient(){
        toothfilling();
        toothExtraction();
    }

    public void toothExtraction(){
        System.out.println("Tooth extraction");
    }
    
    public void toothfilling(){
        System.out.println("Tooth filling proceedure");
    }
}