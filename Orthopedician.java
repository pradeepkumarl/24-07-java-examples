public class Orthopedician extends Doctor {

    public Orthopedician(String name, int age){
        super(name, age);
    }

    @Override
    public void treatPatient(){
        conductCTScan();
        conductXRay();
    }

    public void conductXRay(){
        System.out.println("Conducting X-Ray");
    }

    public void conductCTScan(){
        System.out.println("Conducting CT Scan");
    }
    
}
