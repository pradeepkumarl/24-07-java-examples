import java.util.Scanner;

public class SavingsAccountClient {

    public static void main(String[] args) {
        //data type variable name = new Data-type
       /* SavingsAccount rameshAccount = new ICICI("Ramesh", "ramesh@gmail.com");
        rameshAccount.deposit(2000);
        rameshAccount.deposit(3000);
        double currentBalance = rameshAccount.checkBalance();
        System.out.println("Current balance in Ramesh account is: "+ currentBalance);
        rameshAccount.withdraw(200);
        currentBalance = rameshAccount.checkBalance();
        System.out.println("Current balance in Ramesh account after the withdrawal is: "+ currentBalance);
        String userDetails = rameshAccount.userDetails();
        System.out.println(userDetails);

        SavingsAccount vinayAccount = new ICICI("Vinay", "vinay@gmail.com");
        vinayAccount.deposit(2000);
        vinayAccount.deposit(3000);
         vinayAccount.deposit(5000);
        double vinayAccountBalance = vinayAccount.checkBalance();
        System.out.println("Current balance in Vinay account is: "+ vinayAccountBalance);
        vinayAccount.withdraw(200);
        vinayAccountBalance = vinayAccount.checkBalance();
        System.out.println("Current balance in Vinay account after the withdrawal is: "+ vinayAccountBalance);
        String vinayDetails = vinayAccount.userDetails();
        System.out.println(vinayDetails);*/

        SavingsAccount savingsAccount ;

        Scanner sc = new Scanner(System.in);

        System.out.println("Enter your name");
        String name = sc.next();
        System.out.println("Enter your email");
        String email = sc.next();
        System.out.println("Enter 1 for ICICI and 2 for HDFC");

        int option = sc.nextInt();
        if(option == 1){
            savingsAccount = new ICICI(name, email);
        } else {
            savingsAccount = new HDFC(name, email);
        }

         savingsAccount.deposit(2000);
        savingsAccount.deposit(3000);
         savingsAccount.deposit(5000);
        double savingsAccountBalance = savingsAccount.checkBalance();
        System.out.println("Current balance in Vinay account is: "+ savingsAccountBalance);
        String userDetails = savingsAccount.userDetails();
        System.out.println(userDetails);
        sc.close();
    }
}
