public class Car {

    private double speed;
    private String color;

    public void accelerate(){
        this.speed++;
        this.speed++;
    }

    public void slowDown(){
        this.speed--;
    }

    public void halt(){
        this.speed = 0;
    }

    public double getCurrentSpeed(){
        return this.speed;
    }
    
}
