public class Construct {

    public static void main(String[] args) {
       
        boolean value = true;
        int age = 12;

        if (value){
            //perform the logic if the condition is true
        } else {
            //perform the logic if the condition is otherwise
        }

        switch (age) {
            case 18:
                //statement to be written here
                break;
            case 20:
                //statement to be written here
                break;
            case 25:
                //statement to be written here
                break;
            default:
                break;
        }
        //loop
        for(int index = 0; index < 10; index++){
            System.out.println("Index is "+ index);
        }

        int val = 10;

        while(val >= 0){
            //perform the logic until the condition holds good
            val--;
        }
    }
    
}
