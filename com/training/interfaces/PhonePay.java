package com.training.interfaces;

public class PhonePay implements Pay {

    public double transfer(String from, String to, double money, String notes){
        System.out.println("Transfer of Rs: "+money +" from: "+from+" to: "+to+ " with notes: PhonePay "+ notes);
        return money;
    }

    
}
