package com.training.interfaces;

public class GooglePay implements Pay {

    public double transfer(String from, String to, double money, String notes){
        System.out.println("Transfer of Rs: "+money +" from: "+from+" to: "+to+ " with notes: Google Pay "+ notes);
        return money;
    }

    
}
