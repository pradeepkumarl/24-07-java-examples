package com.training.interfaces;

public interface Pay {

    double transfer(String from, String to, double money, String notes);
    
}
