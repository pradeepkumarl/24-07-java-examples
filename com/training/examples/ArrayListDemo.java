package com.training.examples;

import java.util.ArrayList;
import java.util.List;

public class ArrayListDemo {

    public static void main(String[] args) {
        List<Integer> list = new ArrayList<>();
        for(int index =0; index < 10; index++){
            list.add(index* 41);
        }

        int numberOfElements = list.size();

        boolean doesContains = list.contains(45);

        System.out.println("The size of the array list is ::"+ numberOfElements);
        System.out.println("Does the list container 45: "+ doesContains);

        list.clear();
        System.out.println("Is the list empty: "+ list.isEmpty());


    }
    
}
