public class Opthalmologist extends Doctor {

    public Opthalmologist(String name, int age){
        super(name, age);
    }

    @Override
    public void treatPatient(){
        conductRetinaScan();
    }

    public void conductRetinaScan(){
        System.out.println("Conduction scan on Retina");
    }
    
}
