public class HDFC extends SavingsAccount {

    public HDFC(String name, String email, double initialAccountBalance){
        super(name, email, initialAccountBalance);
    }

    public HDFC(String name, String email){
        super(name, email);
    }

    public String userDetails(){
        return "HDFC user details : "+super.getUser() + " Email: "+ super.getEmail();
    }
    
}
