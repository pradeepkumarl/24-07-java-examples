public class Comment {

    /**
     * This is called a documentation comment
     * @version 1.0.0
     * @param args
     * @since 1.0.0
     */
    public static void main(String[] args) {
        // this is called in line comment 

        //the following command sets the value of the flag
        boolean flag = true;

        /*
         * This is a multi line comment 
         * This can be used to comment out old code
         * This can be used to provide some meaningful information 
         */
    }    
    
}
