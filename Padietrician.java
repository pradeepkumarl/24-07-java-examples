public class Padietrician extends Doctor {

    public Padietrician(String name, int age){
        super(name, age);
    }

    @Override
    public void treatPatient(){
        treatKids();
    }

    public void treatKids(){
        System.out.println("treating kids ");
    }
}
