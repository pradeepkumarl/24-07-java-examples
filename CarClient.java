public class CarClient {

    public static void main(String[] args) {
        Car car = new Car();

        car.accelerate();
        car.accelerate();
        car.accelerate();
        car.accelerate();
        car.accelerate();

        System.out.println("The current speed is: "+ car.getCurrentSpeed());

        car.slowDown();
        car.slowDown();

        System.out.println("The current speed is: "+ car.getCurrentSpeed());

        car.halt();

        System.out.println("The current speed is: "+ car.getCurrentSpeed());
    }
    
}
