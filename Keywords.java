public class Keywords {

    public static void main(String[] args) {
        /**
         * There are more than 60 keywords in java
         *  public, static, if, else, try, catch, void, private
         * 
         */

        //the data has to be assigned to a variable 
        // There are some rules to assign the data to a variable
        /*
         * The format to assign the data to variable
         * datatype variablename = data;
         * The variable name cannot be a keyword
         * The variable name can contain lowercase, uppercase, letters and special chars ( _ and $ )
         * The variable name cannot start with a number
         * 
         */

         /*
          * Conventions used for variable names
          * The name should start with lowercase and should follow camelcase conventions 
          * Use meaningfule variable name
          */

         int data = 20;
         boolean value = true;
    }
}
