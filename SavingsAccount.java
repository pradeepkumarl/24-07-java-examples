public abstract class SavingsAccount {
    //data is private and hidden
    //these are instance variables
    private double balance;
    private String user;
    private String email;


    //expose the functions to act the data
    //overloaded constructor
    /** same constructor but different number of arguments */
    /*
     * By default, the compiler provides a no argument constructor
     * If you provide a constructor, then the compiler will not provide a default no args constructor
     *
     *
     *
     */
    public SavingsAccount(String name, String email, double initialAccountBalance){
        this.user = name;
        this.email = email;
        this.balance = initialAccountBalance;
    }

    public SavingsAccount(String name, String email){
        this.user = name;
        this.email = email;
    }


    // instance methods
    public final void deposit(double money){
        this.balance = this.balance + money;
    }

    public final double withdraw(double money){
        if (this.balance - money > 0){
            this.balance = this.balance -money;
            return money;
        } 
        return 0;
    }


    public final double checkBalance(){
        return this.balance;
    }

    public abstract String userDetails();

    public final String getUser(){
        return this.user;
    }

    public final String getEmail(){
        return this.email;
    }
}
