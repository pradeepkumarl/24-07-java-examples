public abstract class Doctor {

    private String name;
    private int age;

    public Doctor(String name, int age){
        this.name = name;
        this.age = age;
    }
    //marking the method as final prevents it by overriding
    public final void printDoctorDetails(){
        System.out.println("Doctor name: "+name + " Age: "+age);
    }

    //marking the method as abstract forces the subclasses to override
    public abstract void treatPatient();
    
}
